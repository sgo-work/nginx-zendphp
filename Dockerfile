FROM docker.io/nginx:1.24-bullseye

RUN apt update && apt install curl gpg vim -y && sed  -i 's/user  nginx;/user  www-data;/g' /etc/nginx/nginx.conf && \
	curl -s https://repos.zend.com/zend.key | gpg --dearmor > /usr/share/keyrings/zend.gpg && \
	echo "deb [signed-by=/usr/share/keyrings/zend.gpg] https://repos.zend.com/zendphp/deb_debian11/ zendphp non-free" > /etc/apt/sources.list.d/zendphp.list && \
	apt update && apt install php8.2-zend-fpm php8.2-zend-mysql supervisor -y

COPY config/conf.d/default.conf /etc/nginx/conf.d/default.conf
COPY config/supervisord.conf /etc/supervisor/conf.d/supervisord.conf

RUN PHP_VERSION=$(php8.2-zend -v | head -n1 | cut -d' ' -f2 | cut -d. -f1-2) \
	&& mkdir -p /tmp/sourceguardian \
	&& cd /tmp/sourceguardian \
	&& curl -Os https://www.sourceguardian.com/loaders/download/loaders.linux-x86_64.tar.gz \
	&& tar xzf loaders.linux-x86_64.tar.gz \
	&& cp ixed.${PHP_VERSION}.lin "$(php8.2-zend -i | grep '^extension_dir =' | cut -d' ' -f3)/sourceguardian.so" \
	&& printf "; priority=20\nextension=sourceguardian.so\n" > /etc/php/8.2-zend/fpm/conf.d/20-sourcegiardian.ini \
	&& rm -rf /tmp/sourceguardian

EXPOSE 80

CMD ["/usr/bin/supervisord", "-c", "/etc/supervisor/conf.d/supervisord.conf"]